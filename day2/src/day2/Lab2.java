package day2;

public class Lab2 {
	/*
	 * 1.��С�� method ������� bark ����� ������� method main ����¡ method bark
	 * �� method bark ����� local variable ���� dogName
	 * ����˹�������������á��� ���ǡ���� print ��� �The Dog name = xxx bark�
	 */
	public static void main(String[] args) {
		bark();
		bark();

		// 2. ����ͧ����¹��� primitive ���� print �͡�� ���������¹�ҡ

		// Float ໺��¹�� Int

		float numFloat = 300.20f;
		System.out.println("This IS Float " + numFloat);
		int changeToInt = (int) numFloat;
		System.out.println("This Is Float Change To INT " + changeToInt);

		// Int ����¹�� Float

		int numInt = 500;
		System.out.println("This IS INT " + numInt);
		float changeToFloat = (float) numInt;
		System.out.println("This IS INT Change to Float " + changeToFloat);

		// Double ����¹�� Float

		double numDouble = 800.0d;
		System.out.println("This Is Double " + numDouble);
		float changeFloatToDouble = (float) numDouble;
		System.out.println("This IS Double To Float " + changeFloatToDouble);

		// Char ����¹�� Int

		char myChar = '\u0002';
		System.out.println("This Is Char " + myChar);
		int changeCharToInt = (int) myChar;
		System.out.println("This Is Char Change To Int " + changeCharToInt);

		// 3. ���ͧ��С�� final variable ���� hello ����դ�� = �Hello�
		// ���Ǻ�÷Ѵ�Ѵ���ͧ� Assign ������� ������ù���繤�� �World� �����ͧ�� run
		// ���Ǵ�����Դ�â�� ?
		
		final String hello = "hello";
		// hello = "hello"; //Error �����繵���� Final ������ա������¹�ŧ�ա

	}

	public static void bark() {
		String dogname = "Mali";

		System.out.println("The Dog Name " + dogname + " Bark");
	}

}